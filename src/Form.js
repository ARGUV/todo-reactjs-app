import React, {Component} from 'react';

class Form extends Component {
    constructor(props) {
        super(props);

        this.initialState = {
            name: '',
            date: '',
            isShowForm: this.props.isShowForm,
            errors: {},
            readOnly: true
        };

        this.state = this.initialState;
    }

    hideComponent(status) {
        this.setState({isShowForm: !status});
    }

    handleChange = event => {
        const {name, value} = event.target;

        this.setState({
            [name]: value
        });

        this.handleValidation({
            [name]: value
        });
    }

    handleValidation = dataState => {
        const {name, date} = dataState
        let errors = {};
        let formIsValid = true;

        if (typeof name !== 'undefined') {
            if (name.length < 5) {
                formIsValid = false;
                errors["name"] = "Name can't be less 5 letters";
                this.setState({readOnly: true});
            } else {
                this.setState({readOnly: false});
            }
        }

        if (typeof date !== 'undefined') {
            let re = /^\d{4}\-\d{1,2}\-\d{1,2}$/;

            if (!date.match(re)) {
                formIsValid = false;
                errors["date"] = "Wrong date format";
            }
        }
        this.setState({errors: errors});
        return formIsValid;
    }

    onFormSubmit = event => {
        event.preventDefault();

        if (this.handleValidation(this.state)) {
            this.props.handleSubmit(this.state);
            this.props.handleShowForm(this.state.isShowForm);
            this.setState(this.initialState);
        }
    }

    render() {
        const {name, date, isShowForm, readOnly} = this.state;

        return (
            <form onSubmit={this.onFormSubmit}>
                <div className="form-row">
                    <div className="form-group col-md-5">
                        <input
                            className="form-control"
                            type="text"
                            name="name"
                            placeholder="Name"
                            value={name}
                            onChange={this.handleChange}
                        />
                        <span className="text-danger" role="alert">{this.state.errors["name"]}</span>
                    </div>
                    <div className="form-group col-md-5">
                        <input
                            className="form-control"
                            type="date"
                            name="date"
                            placeholder="End Date (date format mm/dd/yyyy)"
                            value={date}
                            onChange={this.handleChange}
                            readOnly={readOnly}
                        />
                        <span className="text-danger" role="alert">{this.state.errors["date"]}</span>
                    </div>
                    <div className="form-group col-md-2">
                        <button className="btn btn-success" type="submit" onClick={() => this.hideComponent(!isShowForm)}>
                            Submit
                        </button>
                    </div>
                </div>
            </form>
        );
    }
}

export default Form;