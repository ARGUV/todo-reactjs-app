import React, {Component} from 'react';
import "bootstrap/dist/css/bootstrap.min.css";

import Table from './Table';
import Form from './Form';

class App extends Component {
    state = {
        characters: [],
        isShowForm: false,
    };

    hideComponent(status) {
        this.setState({isShowForm: !status});
    }

    removeCharacter = index => {
        const {characters} = this.state;

        this.setState({
            characters: characters.filter((character, i) => {
                return i !== index;
            })
        });
    }

    handleSubmit = character => {
        this.setState({characters: [...this.state.characters, character]});
    }

    handleShowForm = isShowForm => {
        this.setState({isShowForm: isShowForm});
    }

    render() {
        const {characters, isShowForm} = this.state;

        return (
            <div className="container">
                <h1>Todo List App in React</h1>

                <div className="row mb-5">
                    <button onClick={() => this.hideComponent(isShowForm)} className="btn btn-success ml-auto">
                        + Add New Todo
                    </button>
                </div>

                <div className="row">
                    {isShowForm ? (
                        <Form
                            handleSubmit={this.handleSubmit}
                            handleShowForm={this.handleShowForm}
                        />
                    ) : (
                        <div></div>
                    )}
                </div>

                <div className="row">
                    <Table
                        characterData={characters}
                        removeCharacter={this.removeCharacter}
                    />
                </div>
            </div>
        );
    }
}

export default App;