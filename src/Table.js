import React from 'react';

const TableHeader = () => {
    return (
        <thead>
        <tr>
            <th scope="col">Name</th>
            <th scope="col">End Date</th>
            <th scope="col">Remove</th>
        </tr>
        </thead>
    );
}

const TableBody = props => {
    const rows = props.characterData.map((row, index) => {
        return (
            <tr key={index}>
                <td>{row.name}</td>
                <td>{row.date}</td>
                <td>
                    <button className="btn btn-success btn-sm" onClick={() => props.removeCharacter(index)}>
                        Delete
                    </button>
                </td>
            </tr>
        );
    });

    return <tbody>{rows}</tbody>;
}

const Table = (props) => {
    const {characterData, removeCharacter} = props;

    return (
        <table className="table table-hover table-bordered">
            <TableHeader/>
            <TableBody characterData={characterData} removeCharacter={removeCharacter}/>
        </table>
    );
}

export default Table;